[Documentation Home](README.md)<br>

# Homedash Modbus Library

This library is a simple Class 0 Modbus client implementation meant for communications with solar energy equipment. The library implements Function Codes 03 and 16 and provides many utility methods specific to interacting with Inverters, MPPT, and OEM LiPo BMS. This library takes a different approach by hiding the Modbus operations in favor of a device value based API.

This library is complemented by the Homedash [Energy System Model](https://gitlab.com/ctkjose/homedash-docs/-/blob/main/SolarEnergyModel.md) which attempts to simplify sharing device configurations and interoperability of DIY projects. A model is a simple json file that describes a device and its values so instead of handling registers you load a device model and query the device properties by name, managing types and conversions is done by the library.

We also have a Google [Spreadsheet](https://docs.google.com/spreadsheets/d/1HIsqBKNrTbUYohOHCB2UrBsVKmLbG7N9JGywD0gBtXw/edit?usp=sharing) meant to serve as a repository of Modbus configurations for different devices. The spreadsheet has a nice macro that lets you create a Model file from the selected rows.




# Modbus Device Specifications

A **Modbus Map** is a document provided by the manufacturer of a device that details their particular implementation of the Modbus protocol and the information on device-specific Modbus registers.

We can use the same generalization of computer memory to understand registers. A device implements a data block that is divided in compartments of 16-bit spaces. These compartments are the registers and the address is the location of the register relative to the block start.

There are two block spaces, the protocol space and the data space (or the common block). We are interested in the data space. Each space has a base (start) address. Commonly the protocol space is 40000 and the data space on 40001. (We refer to these addresses as base address).

The documentation will tell you the actual base address but for most cases we are safe to assume the base address is 40001.

There are two types of registries in the data space; **Input Registers** are read-only and **Holding Registers** are Read-Write.

We use two particular functions of the Modbus protocol to read and write registers. The function code 03 (FC03)(0x03) is used to read a Holding Register and function code 16 (FC16)(0x10) to write to a registry.

We reference a register by its offset on the data block (space). The first register is at offset 0 and the 10th register is at offset 9.

When we mention a **register address** we have to automatically thing on terms of offset. Here is where things get confusing.

When you read a modbus map for a device the manufacturer may list the register address as actual absolute addresses for example 40020 or 40158. In this case if the base data address is 40001 then address `40020` becomes offset 19 (0x13) and we enter 19 as our register address in our Modbus configuration. The actual calculation is:

**Absolute Address** 40020 - **Base Address** 40001 = **Offset** 19

> Captcha #1: Some documents will state that the address are "offsets" of the base address. In which case a listed value of 152 becomes offset 151.

> Captcha #2: The modbus device may use a different base address.

Luckily many modbus maps may already list the actual offsets.

In my experience in the case of Inverters, MPPT and BMS we can make a couple of safe assumptions when the documentation is not clear: the us of base address 40001 and that register values shown in hex are offsets.


> For more details read the [Modbus Application Protocol](https://modbus.org/docs/Modbus_Application_Protocol_V1_1b.pdf).

# Values and Endianess

MODBUS uses a ‘big-Endian’ representation for addresses and data items. This means that when a numerical quantity larger than a single byte is transmitted, the most significant byte is sent first. For example the 16-bit value `0x1234`  will transmit `0x12` first then `0x34`.

In Modbus we store values in a 16 bit (2 bytes) register. For this reason smallest number (UInt8 or SInt8)and character (ASCII) data is stored in 16 bits (2 byte). For example:

| Dec | Hex | Modbus |
| --- | --- | --- |
| 10 | 0x0A | `0x00 0x0A` |
| 1500 | 0x05DC | `0x05 0xDC` |


32 bits numbers (Long, Float) are converted to two 16 bits numbers (Short). When storing a 32 bit number the same rule applies as with 16 bits Short values with the particularity that now we use Word swapping. Each 16 bit Short is converted to a two byte word and we swap the order the low order word  first and the high order word last.

Two's complement is the standard way of representing signed integers.


# Protocol

A modbus message can be sent as ASCII (RS485), RTU (binary over RS485) or binary over TCP.

We support RTU over TCP also known as "Modbus TCP". Modbus TCP encapsulates standard **Modbus RTU** request and response data packets minus the error checking (CRC) in a TCP packet. The default TCP/IP clients and servers listen and receive Modbus data via port 502.

> The guys in IPC2U.com have a great [article](https://ipc2u.com/articles/knowledge-base/detailed-description-of-the-modbus-tcp-protocol-with-command-examples/#desc) on Modbus over TCP if you are interested on the details.

The first 7 Bytes is the Modbus Application Protocol (MBAP) Header.


| Bytes | Field | Size | Description |
| --- | --- | --- | --- |
| 0-1 | Transaction Id | 2 bytes | A 16-bit unique transaction identifier. |
| 2-3 | Protocol Id | 2 bytes | Usual value is zero. |
| 4-5 | length | 2 bytes | The number of bytes that follows. Includes the unit identifier byte, function code byte, and the PDU. |
| 6 | Unit ID/Slave-Address | 1 byte | slave address. Valid range 1-254, 0 and 255 are reserved. |

The MBAP is followed by Protocol Data Unit (PDU) which varies in size up to 255 bytes.

| Bytes | Field | Size | Description |
| --- | --- | --- | --- |
| 7 | Function Code | Byte | Modbus function code. ex: FC3 = 0x03, FC16 = 0x10 |
| 8... | Data | x Bytes | protocol specific data |

> The two bytes for a CRC checksum are not used when using TCP.

Each Modbus over TCP/IP payload must be 256 bytes or less. Proxied devices may take up to five seconds to respond to a request.

# Function Code FC03, Read Holding Registers


| Bytes | Field | Type | Description |
| --- | --- | --- | --- |
| 7 | Function Code | 1 Byte | Modbus function code. FC3 = 0x03 |
| 8-9 | Start Register | 2 Bytes | The First address of the register to read. |
| 10-11 | Register count | 2 Bytes | The number of 16-bit register to read. Valid range is 1 to 124. (0001 - 007Dh) |

Transaction and Protocol ID field values are echoed back to the requesting IP address.



> Xanbus/Conext only support Modbus function-codes 0x03, Read Multiple Registers and 0x10, Write Multiple Registers.

The Start Register Address (Reference Number) are 0 base offsets. Specifications may list data address starting at 40001. These addressed must be converted to the offset for example register `40108` becomes the `107` (40108 - 40001).


## FC3 Example

| Data | Description |
| --- | --- |
| "00 01" | Transaction Id |
| "00 00" | Protocol |
| "46" | Device Id 70 |
| "03" | Read Holding Register |
| "00 06" | Read address 107 (40108-40001 = 0x6B) |
| "00 03" | Read 3 registers (107, 108, 109) |

Response

| Data | Description |
| --- | --- |
| "00 01" | Transaction Id, echoed back |
| "00 00" | Protocol, echoed back |
| "46" | Device Id 70, echoed back |
| "03" | The function code executed or an exception code. If value is the FC the request was succesful. |
| "06" | 6 bytes. The number of bytes that follow (up to 255) |
| "02 2B" | Value of register 107 |
| "00 64" | Value of register 108 |
| "00 7F" | Value of register 109 |

# ComBox Xanbus/Modbus

The Connext Combox can act as a Modbus Server and a Modbus slave. The Modbus address of the Combox is not documented of presented on the Combox' own settings web page, where it list the Modbus addresses of other devices.

The default unit address is `201` on a basic setup with a single Combox. If you familiar with querying system variables on the Combox (using the Combox CGI endpoints) you can find the actual address by listing the value of the variable `MBSERVER.ADDRESS`.

> All the variables available in the ComBox are documented in the file `/xbsysvars.jgz`. Use your browser web inspector to view the content of this file on the web page sources.


# References

https://www.modbustools.com/modbus.html
https://www.fernhillsoftware.com/help/drivers/modbus/modbus-protocol.html
https://ipc2u.com/articles/knowledge-base/detailed-description-of-the-modbus-tcp-protocol-with-command-examples/#desc
https://www.prosoft-technology.com/kb/assets/intro_modbustcp.pdf


https://www.bitzer.de/shared_media/html/ct-310/en-GB/440140939512414603.html


