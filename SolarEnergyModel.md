# Solar Energy Model

This model is meant to serve as a simple mean to standarize data exchange among dashboards, data loggers and others. The model abstracts the particularities of a device and provides a basic dictionary of values to use instead.

The model gets inspiration from the SunSpec's [Models](https://sunspec.org/wp-content/uploads/2022/05/SunSpec-Device-Information-Model-Specificiation-V1-1-final.pdf).


# Common

The **common** schema provides information about a device. It's similar to Sunspec's Model 1.

| Key | Units | Type | Scale | Offset | Description |
| --- | --- | --- | --- | --- | --- |
| common.name |  | str16 |  |  | Device Name |
| common.sn |  | str16 |  |  | Serial Number |
| common.md |  | str20 |  |  | Model |
| common.st |  | enum16 |  |  | Operating State |

# System Stats

|Key|Type|Units|Acces|Scale|Offset|Description|
| --- | --- | --- | --- | --- | --- | --- |
|stat.load.w|uint32||R|||Load Power|
|stat.load.w.max|uint32||R|||Load Max Power|
|stat.load.v|uint32|V|R|0.001||Load Voltage|
|stat.load.hz|uint32|Hz|R|0.01||Load Frequenzy|
|stat.load.a|uint32|A|R|0.001||Load Current|
|stat.load.l1.v|uint32|V|R|0.001||Load Line 1 Voltage|
|stat.load.l2.v|uint32|V|R|0.001||Load Line 2 Voltage|
|stat.load.l1.a|uint32|A|R|0.001||Load Line 1 Current|
|stat.load.l2.a|uint32|A|R|0.001||Load Line 2 Current|
|stat.bat.w|uint32|W|R|||Battery Power Net|
|stat.bat.v|uint32|V|R|0.001||Battery Voltage|
|stat.bat.t|uint32|C|R|0.01|-273|Battery Temperature|
|stat.bat.load.w|uint32|W|R|||Battery Load Power|
|stat.bat.chg.w|uint32|W|R|||Battery Charging Power|
|stat.grid.w|uint32|W|R|||Grid Net Power|
|stat.grid.l1.a|uint32|A|R|0.001||Grid Line 1 Current|
|stat.grid.l2.a|uint32|A|R|0.001||Grid Line 2 Current|
|stat.grid.l1.v|uint32|V|R|0.001||Grid Line 1 Voltage|
|stat.grid.l1.v|uint32|V|R|0.001||Grid Line 2 Voltage|
|stat.pv.w|uint32|W|R|||PV Power|
|stat.pv.v|uint32|V|R|0.001||PV Voltage|
|stat.pv.a|uint32|A|R|0.001||PV Current|
|stat.pv.w.max|uint32|W|R|||PV Total Max Power |
|stat.pv.w.hour|uint32|kWh|R|||PV Total Energy Hourly |
|stat.pv.w.lifetime|uint32|kWh|R|||PV Total Energy Lifetime |




# Data Types

| Type | Description |
| --- | --- |
| "uint16" | unsigned 16-bit integer (word) [0,65535] |
| "uint32" | unsigned 32-bit integer (dword) [0,4294967295] |
| "sint16" | signed 16-bit integer [-2147483648,2147483647] |
| "sint32" | signed 32-bit integer [-2147483648,2147483647] |
| "strXX" | packed 8-bit ASCII string of specified length, where `XX` is the length of characters in the string. eg: "str20" is a 20 byte string. 2 characters are packed in ModBus word. |
| "enum16" | Value is an integer that is the key to the a list of values is defined in the entry "symbols". |
| "enum32" | Value is an integer that is the key to the a list of values is defined in the entry "symbols". |
| "string" | Arbirtrary length UTF-8 string as supported by JSON. |
| "integer" | Arbirtrary size signed/unsigned integer as supported by JSON. |
| "float" | Arbirtrary size signed/unsigned float/double  as supported by JSON. |
| "boolean" | Values `true` or `false` as supported by JSON. |

# Units of Measurement

The schema field `units` describes the measurement units.

| Unit | Description |
| --- | --- |
| W | Watts (Power). Default scale 1 |
| kWh | Kilo-Watts Hour (Power). Default scale 0.001 |
| A | Amperage (Current). Default scale 0.001 |
| V | Voltage. Default scale 0.001 |
| VA | Volt-Ampere. (Apparent Power, AC) Default scale 1 |
| Hz | Frequency in Hertz. Default scale 0.01 |
| s | Seconds. Default scale 1 |
| min | Minutes. Default scale 1 |
| deg | Degrees in Celsius. Default scale 0.001, offset -273.0 |
| % | Percentage. Default scale 1 |

As an alternative to floating-point format, values are represented by 32 bit integer values with a signed `scale` factor applied. A negative scale factor explicitly shifts the decimal point to the left, and a positive scale factor shifts the decimal point to the right by the number of places specified in the scale factor value.

Data from a Modbus register is converted to units of measurement using the following algorithm:

```
result = [value * scale] + offset
```
